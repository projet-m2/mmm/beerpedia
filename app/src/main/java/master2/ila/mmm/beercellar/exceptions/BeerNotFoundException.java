package master2.ila.mmm.beercellar.exceptions;

public class BeerNotFoundException extends Exception {
    public BeerNotFoundException(String reason){
        super(reason);
    }
}
