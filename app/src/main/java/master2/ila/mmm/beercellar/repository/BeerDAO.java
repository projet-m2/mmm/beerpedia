package master2.ila.mmm.beercellar.repository;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import master2.ila.mmm.beercellar.domain.Beer;
import master2.ila.mmm.beercellar.exceptions.BeerNotAddedException;
import master2.ila.mmm.beercellar.exceptions.BeerNotFoundException;

public class BeerDAO {
    public static final String TAG = "cloudstore";
    private static BeerDAO _instance;

    private final String BEER_COLLECTION = "beers";

    private FirebaseFirestore db;
    private CollectionReference beers;

    public static BeerDAO getInstance() {
        if (_instance == null)
            _instance = new BeerDAO();
        return _instance;
    }

    private BeerDAO() {
        this.db = FirebaseFirestore.getInstance();
        this.beers = db.collection(BEER_COLLECTION);
    }

    public CompletableFuture<List<Beer>> getBeers(int offset, int limit) {
        Log.d(TAG, String.format("Request to database: offset=%d, limit=%d", offset, limit));
        CompletableFuture<List<Beer>> f = new CompletableFuture<>();
        beers.orderBy("name").startAt(offset).limit(limit).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                List<Beer> res = task.getResult().toObjects(Beer.class);
                Log.d(TAG, String.format("Database query was successful, got %d results", res.size()));
                f.complete(res);
            } else {
                Log.w(TAG, "Failed query");
                f.completeExceptionally(new BeerNotFoundException("Beer was not found :("));
            }
        });
        return f;
    }

    public CompletableFuture<Beer> getBeerById(String id) {
        Log.d(TAG, String.format("Querying for beer %s", id));
        CompletableFuture<Beer> f = new CompletableFuture<>();
        beers.document(id).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Beer res = task.getResult().toObject(Beer.class);
                Log.d(TAG, String.format("Database query was successful, got object with id %s", res.getId()));
                f.complete(res);
            } else {
                f.completeExceptionally(new BeerNotFoundException("Beer was not found :("));
                Log.w(TAG, "Failed query");

            }
        });
        return f;
    }

    public CompletableFuture<String> addBeer(Beer beer) {
        Log.d(TAG, "addBeer: Adding beer");
        CompletableFuture<String> f = new CompletableFuture<>();
        beers.document(beer.getId()).set(beer).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d(TAG, String.format("Object id=%s was successfully added to database", beer.getId()));
                f.complete(beer.getId());
            } else {
                f.completeExceptionally(new BeerNotAddedException("Beer was not added :'("));
                Log.w(TAG, "Failed query");
            }
        });
        return f;
    }

    public CompletableFuture<String> updateUserNoteById(String id, Map<String,Double> mapUserNote) {
        Log.d(TAG, String.format("Querying to update beer %s", id));
        CompletableFuture<String> f = new CompletableFuture<>();
        beers.document(id).update("notes", mapUserNote).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d(TAG, String.format("Database query was successful, updated note with id %s", id));
                f.complete(id);
            } else {
                f.completeExceptionally(new BeerNotFoundException("Beer was not found :("));
                Log.w(TAG, "Failed query");
            }
        });
        return f;
    }

    public CompletableFuture<String> updateFavoritesById(String id, List<String> favorites) {
        Log.d(TAG, String.format("Querying to update beer %s", id));
        CompletableFuture<String> f = new CompletableFuture<>();
        beers.document(id).update("favorites", favorites).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d(TAG, String.format("Database query was successful, updated note with id %s", id));
                f.complete(id);
            } else {
                f.completeExceptionally(new BeerNotFoundException("Beer was not found :("));
                Log.w(TAG, "Failed query");
            }
        });
        return f;
    }
}
