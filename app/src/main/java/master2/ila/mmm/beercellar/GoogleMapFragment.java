package master2.ila.mmm.beercellar;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import master2.ila.mmm.beercellar.domain.Beer;

public class GoogleMapFragment extends Fragment {

    public Beer beer;


    public GoogleMapFragment() {
        // Required empty public constructor
    }

    public GoogleMapFragment(Beer beer) {
        this.beer = beer;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_google_map, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);  //use SuppoprtMapFragment for using in fragment instead of activity  MapFragment = activity   SupportMapFragment = fragment
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                googleMap.clear();

                LatLng beerSelectedLatLng = new LatLng(0, 0);
                List<Double> coordinates = beer.getCoordinates();
                if (coordinates != null) {
                    beerSelectedLatLng = new LatLng(coordinates.get(0), coordinates.get(1));
                }

                CameraPosition cameraPosition = CameraPosition.builder()
                        .target(beerSelectedLatLng)
                        .zoom(5)
                        .bearing(0)
                        .tilt(0)
                        .build();

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 500, null);

                googleMap.addMarker(new MarkerOptions()
                        .position(beerSelectedLatLng)
                        .title(beer.getBrewery()));
            }
        });

        return rootView;
    }

    public LatLng getLatLngFromAdress(String address) {
        Geocoder geocoder = new Geocoder(getActivity());
        List<Address> addressList;
        LatLng latLng = null;

        try {
            addressList = geocoder.getFromLocationName(address, 5);
            if (addressList == null) {
                return latLng;
            }

            Address location = addressList.get(0);

            latLng = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("lat : " + latLng.latitude + "lng :" + latLng.longitude);
        return latLng;
    }
}
