package master2.ila.mmm.beercellar.domain;

import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class Beer {
    String id;
    String name;
    String style;
    Double ibu;
    String description;
    String category;
    String city;
    String country;
    List<Double> coordinates;
    String state;
    Double abv;
    String address;
    String brewery;
    Map<String, Double> notes;
    Double average_note;
    String imageURI;
    List<String> favorites;
}