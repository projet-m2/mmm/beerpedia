package master2.ila.mmm.beercellar.Services;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import master2.ila.mmm.beercellar.domain.Beer;
import master2.ila.mmm.beercellar.repository.BeerDAO;

import static com.google.common.hash.Hashing.goodFastHash;

public class BeerService {
    private BeerDAO beers;

    private final String TAG = "BeerService";

    public BeerService() {
        this.beers = BeerDAO.getInstance();
    }

    public CompletableFuture<List<Beer>> getBeers(int offset, int limit) {
        return this.beers.getBeers(offset, limit);
    }

    public CompletableFuture<Beer> getBeerById(String id) {
        return this.beers.getBeerById(id);
    }

    public CompletableFuture<String> addBeer(@NotNull Beer beer) {
        if (beer.getId() == null) {
            beer.setId(goodFastHash(128).hashInt(beer.hashCode()).toString());
        }
        return this.beers.addBeer(beer);
    }

    public CompletableFuture<String> updateBeer(@NotNull String id, Map<String,Double> mapUserNote) {
        return this.beers.updateUserNoteById(id, mapUserNote);
    }

    public CompletableFuture<String> updateBeer(@NotNull String id, List<String> favorites) {
        return this.beers.updateFavoritesById(id, favorites);
    }
}
