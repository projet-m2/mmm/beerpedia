package master2.ila.mmm.beercellar;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import master2.ila.mmm.beercellar.domain.Beer;

public class BeerAdapter extends RecyclerView.Adapter<BeerAdapter.MyViewHolder> implements Filterable {
    private List<Beer> beerList = new ArrayList<>();
    private List<Beer> beerListFiltered = new ArrayList<>();
    private OnBeerListener mOnBeerListener;

    public BeerAdapter(OnBeerListener onBeerListener) {
        this.mOnBeerListener = onBeerListener;
    }

    public void addBeers(List<Beer> beers) {
        beerList.addAll(beers);
        beerListFiltered = beerList;
        notifyDataSetChanged();
    }

    public Beer getBeerAt(int adapterPosition) {
        return beerListFiltered.get(adapterPosition);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_beer_item, parent, false);

        return new MyViewHolder(itemView, mOnBeerListener);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Beer currentBeer = beerListFiltered.get(position);
        holder.name.setText(currentBeer.getName());

        if (!"".equals(currentBeer.getImageURI())) {
            StorageReference storageReference = FirebaseStorage.getInstance().getReferenceFromUrl(currentBeer.getImageURI());

            final long ONE_MEGABYTE = 1024 * 1024;

            storageReference.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    holder.image.setImageBitmap(bitmap);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return beerListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    beerListFiltered = beerList;
                } else {
                    List<Beer> filteredList = new ArrayList<>();
                    for (Beer row : beerList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    beerListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = beerListFiltered;
                System.out.println(charSequence);
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                beerListFiltered = (ArrayList<Beer>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface OnBeerListener {
        void onBeerClick(int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name;
        public ImageView image;
        OnBeerListener onBeerListener;

        public MyViewHolder(View view, OnBeerListener onBeerListener) {
            super(view);
            name = view.findViewById(R.id.textViewBeerName);
            image = view.findViewById(R.id.imageBeer);
            this.onBeerListener = onBeerListener;
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onBeerListener.onBeerClick(getAdapterPosition());
        }
    }
}
