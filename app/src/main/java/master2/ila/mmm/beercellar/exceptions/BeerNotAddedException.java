package master2.ila.mmm.beercellar.exceptions;

public class BeerNotAddedException extends Exception {
    public BeerNotAddedException(String reason){
        super(reason);
    }
}
