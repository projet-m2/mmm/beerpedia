package master2.ila.mmm.beercellar;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.SneakyThrows;
import master2.ila.mmm.beercellar.Services.BeerService;
import master2.ila.mmm.beercellar.domain.Beer;

public class SelectedBeer extends AppCompatActivity {

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.style)
    TextView style;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.ibu)
    TextView ibu;
    @BindView(R.id.abv)
    TextView abv;
    @BindView(R.id.localisation)
    TextView localisation;
    @BindView(R.id.brewery)
    TextView brewery;
    @BindView(R.id.rating_average)
    TextView rating_average;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    @BindView(R.id.linear1)
    LinearLayout linearLayout1;

    BeerService beerService;

    Beer selectedBeer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_beer);

        ButterKnife.bind(this);

        beerService = new BeerService();

        beerService.getBeerById(getIntent().getStringExtra("beerId")).thenAccept(this::setSelectedBeer);
    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        return true;
    }

    private void setSelectedBeer(Beer beer) {
        selectedBeer = beer;
        fillContent(selectedBeer);
    }

    private void fillContent(Beer beer) {
        textViewFillWithObjectOrRemove(name, beer.getName());
        textViewFillWithObjectOrRemove(description, beer.getDescription());
        textViewFillWithObjectOrRemove(style, beer.getStyle());
        textViewFillWithObjectOrRemove(category, beer.getCategory());
        textViewFillWithObjectOrRemove(ibu, beer.getIbu());
        textViewFillWithObjectOrRemove(abv, beer.getAbv() + "%");
        if (!"".equals(beer.getState())) {
            textViewFillWithObjectOrRemove(localisation, beer.getState() + " - " + beer.getCountry() + " - " + beer.getCity() + " - " + beer.getAddress());

        } else {
            textViewFillWithObjectOrRemove(localisation, beer.getCountry() + " - " + beer.getCity() + " - " + beer.getAddress());
        }
        textViewFillWithObjectOrRemove(brewery, beer.getBrewery());

        if (!"".equals(beer.getImageURI())) {
            StorageReference storageReference = FirebaseStorage.getInstance().getReferenceFromUrl(beer.getImageURI());

            final long ONE_MEGABYTE = 1024 * 1024;

            storageReference.getBytes(ONE_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    image.setImageBitmap(bitmap);
                }
            });
        }

        invalidateOptionsMenu();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        GoogleMapFragment googleMapFragment = new GoogleMapFragment(beer);
        fragmentTransaction.add(R.id.fragment_map, googleMapFragment);
        fragmentTransaction.commit();

        String noteUserId = "note_" + FirebaseAuth.getInstance().getCurrentUser().getUid();
        Map<String, Double> notes = beer.getNotes();
        if (notes.containsKey(noteUserId)) {
            ratingBar.setRating(notes.get(noteUserId).floatValue());
        }

        Double average = notes.values().stream().reduce((double) 0, Double::sum) / notes.keySet().size();

        textViewFillWithObjectOrRemove(rating_average, "\n" + average);

    }

    private void textViewFillWithObjectOrRemove(TextView textView, Object object) {
        if (object != null && !object.toString().equals("")) {
            textView.append(object.toString());
        } else {
            linearLayout1.removeView(textView);
        }
    }

    public void submitNote(View view) {
        String noteUserId = "note_" + FirebaseAuth.getInstance().getCurrentUser().getUid();
        selectedBeer.getNotes().put(noteUserId, new Double(ratingBar.getRating()));
        beerService.updateBeer(selectedBeer.getId(), selectedBeer.getNotes());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_selected, menu);
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        if(selectedBeer != null){
            if (selectedBeer.getFavorites().contains(userId)) {
                menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_favorite_added));
            }else{
                menu.getItem(0).setIcon(ContextCompat.getDrawable(this, R.drawable.ic_favorite));
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_favourite:
                String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                if (selectedBeer.getFavorites().contains(userId)) {
                    item.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_favorite));
                    selectedBeer.getFavorites().remove(userId);
                } else {
                    item.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_favorite_added));
                    selectedBeer.getFavorites().add(userId);
                }
                beerService.updateBeer(selectedBeer.getId(), selectedBeer.getFavorites());
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

}
