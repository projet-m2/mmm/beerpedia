#!/usr/bin/python
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import json
from pprint import pprint

# Use a service account
cred = credentials.Certificate('beerpedia-key.json')
firebase_admin.initialize_app(cred)

db = firestore.client()
col_ref = db.collection(u'beers')


def get_or_null(d, k):
    if k in d:
        return d[k]
    else:
        return ""


def get_coordinates(d, k):
    coor = get_or_null(d, k)
    if(coor == ''):
        return None
    coor.extend(["0", "0"])
    res = []
    res.append(float(coor[0]))
    res.append(float(coor[1]))
    return res


def map_beer(beer):
    # pprint(beer)
    res = {}
    res['id'] = beer['recordid']
    res['name'] = get_or_null(beer['fields'], 'name')
    res['style'] = get_or_null(beer['fields'], 'style_name')
    res['ibu'] = get_or_null(beer['fields'], 'ibu')
    res['description'] = get_or_null(beer['fields'], 'descript')
    res['category'] = get_or_null(beer['fields'], 'cat_name')
    res['city'] = get_or_null(beer['fields'], 'city')
    res['country'] = get_or_null(beer['fields'], 'country')
    res['coordinates'] = get_coordinates(beer['fields'], 'coordinates')
    res['state'] = get_or_null(beer['fields'], 'state')
    res['abv'] = get_or_null(beer['fields'], 'abv')
    res['address'] = get_or_null(beer['fields'], 'address1')
    res['brewery'] = get_or_null(beer['fields'], 'name_breweries')
    res['notes'] = [3]
    res['average_note'] = 3
    return res


with open("open-beer-database.json", "r") as f:
    # with open("beer-entry.json","r") as f:

    batch = db.batch()

    data = json.load(f)
    # size = len(data)
    size = 20
    for index, entry in enumerate(map(map_beer, data)):
        if entry['country'] != 'France':
            continue

        print("{} of {}".format(index, size))
        doc_ref = col_ref.document(entry['id'])
        batch.set(doc_ref, entry)
        # doc_ref.set(entry)
    print("uploading")
    batch.commit()
    print("done uploading")

# with open("beer-entry.json","r") as f:
#     data = json.load(f)
#     pprint(json.dumps(map_beer(data[0])))
